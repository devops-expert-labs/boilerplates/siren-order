package io.infograb.order.controller;

import io.infograb.order.model.Menu;
import io.infograb.order.service.MenuService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@DisplayName("Test MenuController")
@WebMvcTest(MenuController.class)
public class MenuControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private MenuService menuService;

    @BeforeEach
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .addFilters(new CharacterEncodingFilter("UTF-8", true))
                //.alwaysDo(print())
                .build();
    }

    @DisplayName("Test retrieving all menus")
    @Test
    public void shouldRetrieveAllMenus() throws Exception {
        // given
        Menu menu1 = new Menu();
        menu1.setId(1);
        menu1.setName("아이스 카페 아메리카노");
        menu1.setEnglishName("Iced Caffe Americano");
        menu1.setPrice(4100);

        Menu menu2 = new Menu();
        menu2.setId(2);
        menu2.setName("아이스 카페 라떼");
        menu2.setEnglishName("Iced Caffe Latte");
        menu2.setPrice(4600);

        Menu menu3 = new Menu();
        menu3.setId(3);
        menu3.setName("아이스 카페 모카");
        menu3.setEnglishName("Iced Caffe Mocha");
        menu3.setPrice(5100);

        List<Menu> menus = new ArrayList<>();
        menus.add(menu1);
        menus.add(menu2);
        menus.add(menu3);

        given(menuService.findMenus()).willReturn(menus);

        // when
        final ResultActions actions = this.mockMvc.perform(get("/menu"));

        // then
        actions
                //.andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("pages/menu"))
                .andExpect(content().string(containsString("아이스 카페 아메리카노")))
                .andExpect(content().string(containsString("아이스 카페 라떼")))
                .andExpect(content().string(containsString("아이스 카페 모카")));
    }

}
