package io.infograb.order.controller;

import io.infograb.order.model.User;
import io.infograb.order.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mybatis.spring.boot.test.autoconfigure.AutoConfigureMybatis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@DisplayName("Test HomeController")
@WebMvcTest(HomeController.class)
@AutoConfigureMybatis
public class HomeControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private UserService userService;

    @BeforeEach
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .addFilters(new CharacterEncodingFilter("UTF-8", true))
                //.alwaysDo(print())
                .build();
    }

    @DisplayName("Test getting a user by ID")
    @Test
    public void shouldGetUserById() throws Exception {
        // given
        User userToReturn = new User();
        userToReturn.setId("mark85");
        userToReturn.setName("마크85");
        userToReturn.setPhone("010-1111-2222");
        userToReturn.setEmail("mark85@infograb.net");
        userToReturn.setUseNickname(false);
        userToReturn.setBalance(5000);
        userToReturn.setRewards(0);

        given(userService.findUserById("mark85")).willReturn(userToReturn);

        // when
        final ResultActions actions = this.mockMvc.perform(get("/?userId=mark85"));

        // then
        actions
                //.andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("pages/home"))
                .andExpect(content().string(containsString("mark")));
    }

}
