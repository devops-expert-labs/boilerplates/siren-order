package io.infograb.order.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.infograb.order.service.OrderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@DisplayName("Test OrderController")
@WebMvcTest(OrderController.class)
public class OrderControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private OrderService orderService;

    @BeforeEach
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .addFilters(new CharacterEncodingFilter("UTF-8", true))
                //.alwaysDo(print())
                .build();
    }

    @DisplayName("Test Order - Success")
    @Test
    public void shouldBeSuccessfulToOrder() throws Exception {
        // given
        Map<String, String> result = new HashMap<>();
        result.put("message", "주문이 완료되었습니다.");

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(result);

        given(orderService.orderDrink("user01", 3)).willReturn(true);

        // when
        final ResultActions actions = this.mockMvc.perform(post("/order?userId=user01&menuId=3")
                .contentType(MediaType.APPLICATION_JSON));

        // then
        actions.andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().json(json))
                .andExpect(MockMvcResultMatchers.jsonPath("message", is("주문이 완료되었습니다.")));
    }

    @DisplayName("Test Order When Insufficient Balance")
    @Test
    public void shouldBeFailedToOrderIfInsufficientBalance() throws Exception {
        // given
        Map<String, String> result = new HashMap<>();
        result.put("message", "잔액이 부족합니다.");

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(result);

        given(orderService.orderDrink("user01", 3)).willReturn(true);
        given(orderService.orderDrink("user01", 3)).willReturn(false);

        // when
        final ResultActions actions = this.mockMvc.perform(post("/order?userId=user01&menuId=3")
                .contentType(MediaType.APPLICATION_JSON));

        // then
        actions.andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().json(json))
                .andExpect(MockMvcResultMatchers.jsonPath("message", is("잔액이 부족합니다.")));
    }

}
