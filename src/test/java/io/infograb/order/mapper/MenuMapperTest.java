package io.infograb.order.mapper;

import io.infograb.order.model.Menu;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;

import java.util.List;

@DisplayName("Test MenuMapper")
@MybatisTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class MenuMapperTest {

    @Autowired
    private MenuMapper menuMapper;

    @DisplayName("Test selectMenus")
    @Test
    public void shouldRetrieveAllMenus() {
        List<Menu> menus = menuMapper.selectMenus();
        Assertions.assertNotNull(menus, "No menus found.");
        Assertions.assertEquals(menus.size(), 3);
    }

    @DisplayName("Test selectMenuById - Success")
    @Test
    public void shouldGetMenuById() {
        Menu menu = menuMapper.selectMenuById(3);
        Assertions.assertNotNull(menu, "The menu has not been found.");
        Assertions.assertEquals(menu.getName(), "아이스 카페 모카");
        Assertions.assertEquals(menu.getPrice(), 5100);
    }

    @DisplayName("Test selectMenuById - Not Found")
    @Test
    public void shouldNotFindMenuById() {
        Menu menu = menuMapper.selectMenuById(30);
        Assertions.assertNull(menu, "The menu should not be found.");
    }

}
