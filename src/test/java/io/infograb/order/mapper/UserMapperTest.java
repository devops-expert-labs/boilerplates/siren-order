package io.infograb.order.mapper;

import io.infograb.order.model.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Test UserMapper")
@MybatisTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class UserMapperTest {

    @Autowired
    private UserMapper userMapper;

    @DisplayName("Test selectUserById - Success")
    @Test
    public void shouldFindUserById() {
        User user = userMapper.selectUserById("user01");
        assertNotNull(user, "The user has not been found");
        assertEquals(user.getId(), "user01");
        assertEquals(user.getName(), "홍길동");
        assertEquals(user.getPhone(), "010-1234-5678");
        assertEquals(user.getEmail(), "gdhong@infograb.net");
        assertTrue(user.isUseNickname());
        assertEquals(user.getNickname(), "신출귀몰");
    }

    @DisplayName("Test findUserById - Not Found")
    @Test
    void shouldNotFindUserById() {
        User user = userMapper.selectUserById("mark85");
        assertNull(user, "The user should not be found");
    }

}
