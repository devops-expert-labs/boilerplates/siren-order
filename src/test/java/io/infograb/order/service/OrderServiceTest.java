package io.infograb.order.service;

import io.infograb.order.mapper.MenuMapper;
import io.infograb.order.mapper.UserMapper;
import io.infograb.order.model.Menu;
import io.infograb.order.model.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;

@DisplayName("Test OrderService")
@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {

    @InjectMocks
    private OrderService orderService;

    @Mock
    private UserMapper userMapper;

    @Mock
    private MenuMapper menuMapper;

    @DisplayName("Test orderDrink - Success")
    @Test
    void shouldBeSuccessfulToOrderDrink() throws Exception {
        // given
        User user = new User();
        user.setId("mark85");
        user.setName("마크85");
        user.setPhone("010-1111-2222");
        user.setEmail("mark85@infograb.net");
        user.setUseNickname(false);
        user.setBalance(15000);
        user.setRewards(0);

        Menu menu = new Menu();
        menu.setId(11);
        menu.setName("다크 초콜릿 모카");
        menu.setEnglishName("Dark Chocolate Mocha");
        menu.setPrice(5500);

        given(userMapper.selectUserById("mark85")).willReturn(user);
        given(menuMapper.selectMenuById(11)).willReturn(menu);

        // when
        boolean result = orderService.orderDrink("mark85", 11);

        // then
        assertTrue(result, "Insufficient Balance");
    }

    @DisplayName("Test orderDrink When Insufficient Balance")
    @Test
    void shouldBeFailedToOrderDrinkIfInsufficientBalance() throws Exception {
        // given
        User user = new User();
        user.setId("mark85");
        user.setName("마크85");
        user.setPhone("010-1111-2222");
        user.setEmail("mark85@infograb.net");
        user.setUseNickname(false);
        user.setBalance(5000);
        user.setRewards(0);

        Menu menu = new Menu();
        menu.setId(11);
        menu.setName("다크 초콜릿 모카");
        menu.setEnglishName("Dark Chocolate Mocha");
        menu.setPrice(5500);

        given(userMapper.selectUserById("mark85")).willReturn(user);
        given(menuMapper.selectMenuById(11)).willReturn(menu);

        // when
        boolean result = orderService.orderDrink("mark85", 11);

        // then
        assertFalse(result);
    }

}
