package io.infograb.order.service;

import io.infograb.order.mapper.UserMapper;
import io.infograb.order.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.BDDMockito.given;

@DisplayName("Test UserService")
@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserMapper userMapper;

    @DisplayName("Test findUserById - Success")
    @Test
    void shouldFindUserById() throws Exception {
        // given
        User actualUser = new User();
        actualUser.setId("mark85");
        actualUser.setName("마크85");
        actualUser.setPhone("010-1111-2222");
        actualUser.setEmail("mark85@infograb.net");
        actualUser.setUseNickname(false);
        actualUser.setBalance(5000);
        actualUser.setRewards(0);

        given(userMapper.selectUserById("mark85")).willReturn(actualUser);

        // when
        User expectedUser = userService.findUserById("mark85");

        // then
        assertNotNull(expectedUser, "The user has not been found.");
        assertEquals(expectedUser, actualUser);
    }

    @DisplayName("Test findUserById - Not Found")
    @Test
    void shouldNotFindUserById() throws Exception {
        User foundUser = userService.findUserById("mark85");
        Assertions.assertNull(foundUser, "The user should not be found.");
    }

}
