package io.infograb.order.service;

import io.infograb.order.mapper.MenuMapper;
import io.infograb.order.model.Menu;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.BDDMockito.given;

@DisplayName("Test MenuService")
@ExtendWith(MockitoExtension.class)
public class MenuServiceTest {

    @InjectMocks
    private MenuService menuService;

    @Mock
    private MenuMapper menuMapper;

    @DisplayName("Test finding all menus")
    @Test
    public void shouldFindAllMenus() throws Exception {
        // given
        Menu menu1 = new Menu();
        menu1.setId(1);
        menu1.setName("아이스 카페 아메리카노");
        menu1.setEnglishName("Iced Caffe Americano");
        menu1.setPrice(4100);

        Menu menu2 = new Menu();
        menu2.setId(2);
        menu2.setName("아이스 카페 라떼");
        menu2.setEnglishName("Iced Caffe Latte");
        menu2.setPrice(4600);

        Menu menu3 = new Menu();
        menu3.setId(3);
        menu3.setName("아이스 카페 모카");
        menu3.setEnglishName("Iced Caffe Mocha");
        menu3.setPrice(5100);

        List<Menu> menus = new ArrayList<>();
        menus.add(menu1);
        menus.add(menu2);
        menus.add(menu3);

        given(menuMapper.selectMenus()).willReturn(menus);

        // when
        List<Menu> expectedMenus = menuService.findMenus();

        // then
        assertNotNull(menus);
        assertEquals(expectedMenus.size(), menus.size());
        assertEquals(expectedMenus, menus);
    }

}
