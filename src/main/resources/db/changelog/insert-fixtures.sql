INSERT INTO `user` (`id`, `name`, `phone`, `email`, `use_nickname`, `nickname`, `balance`, `rewards`)
VALUES
    ('user01', '홍길동', '010-1234-5678', 'gdhong@infograb.net', true, '신출귀몰', 10000, 0);

INSERT INTO `menu` (`id`, `name`, `english_name`, `price`)
VALUES
    (1, '아이스 카페 아메리카노', 'Iced Caffe Americano', 4100),
    (2, '아이스 카페 라떼', 'Iced Caffe Latte', 4600),
    (3, '아이스 카페 모카', 'Iced Caffe Mocha', 5100);
