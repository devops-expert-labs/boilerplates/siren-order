package io.infograb.order.controller;

import io.infograb.order.model.Menu;
import io.infograb.order.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class MenuController {

    private final MenuService menuService;

    @Autowired
    public MenuController(MenuService menuService) {
        this.menuService = menuService;
    }

    @GetMapping("/menu")
    public String getMenus(Model model) throws Exception {
        List<Menu> menus = menuService.findMenus();
        model.addAttribute("menus", menus);
        return "pages/menu";
    }

}
