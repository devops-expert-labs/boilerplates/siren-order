package io.infograb.order.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import io.infograb.order.model.User;
import io.infograb.order.service.UserService;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Controller
public class HomeController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final UserService userService;

    @Autowired
    public HomeController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/")
    public String home(@RequestParam(value = "userId", required = false, defaultValue = "user01") String userId, Model model) throws Exception {
        logger.debug("userId ===== {}", userId);
        User user = userService.findUserById(userId);
        userId = maskString(userId, userId.length() - 2, userId.length(), '*');
        user.setId(userId);
        model.addAttribute("user", user);
        return "pages/home";
    }

    @PostMapping(value = "/recharge", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, String> order(@RequestParam(value = "userId", required = false, defaultValue = "user01") String userId,
                                     @RequestParam(value = "amount", required = false, defaultValue = "10000") int amount) throws Exception {
        logger.debug("userId ===== {}", userId);
        logger.debug("amount ===== {}", amount);
        userService.rechargeBalance(userId, amount);

        Locale locale = new Locale.Builder().setLanguage("ko").setRegion("KR").build();
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
        Map<String, String> result = new HashMap<>();
        result.put("message", currencyFormatter.format(amount) + "원이 충전되었습니다.");
        return result;
    }

    private static String maskString(String text, int start, int end, char maskChar) throws Exception {

        if (text == null || text.equals(""))
            return "";

        if (start < 0)
            start = 0;

        if (end > text.length())
            end = text.length();

        if (start > end)
            throw new Exception("End index cannot be greater than start index");

        int maskLength = end - start;

        if (maskLength == 0)
            return text;

        StringBuilder sbMaskString = new StringBuilder(maskLength);

        for (int i = 0; i < maskLength; i++) {
            sbMaskString.append(maskChar);
        }

        return text.substring(0, start) + sbMaskString.toString() + text.substring(start + maskLength);
    }

}
