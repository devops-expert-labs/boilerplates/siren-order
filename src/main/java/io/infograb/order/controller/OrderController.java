package io.infograb.order.controller;

import io.infograb.order.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class OrderController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping(value = "/order", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, String> order(@RequestParam(value = "userId", required = false, defaultValue = "user01") String userId,
                                     @RequestParam(value = "menuId", required = false, defaultValue = "0") int menuId) throws Exception {
        logger.debug("userId ===== {}", userId);
        logger.debug("menuId ===== {}", menuId);
        boolean success = orderService.orderDrink(userId, menuId);

        Map<String, String> result = new HashMap<>();
        if (success) {
            result.put("message", "주문이 완료되었습니다.");
        } else {
            result.put("message", "잔액이 부족합니다.");
        }
        return result;
    }

}
