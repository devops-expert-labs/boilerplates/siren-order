package io.infograb.order.model;

import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.type.Alias;

@Alias("Menu")
@Getter
@Setter
public class Menu {

    private int id;
    private String name;
    private String englishName;
    private int price;

}
