package io.infograb.order.model;

import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.type.Alias;

@Alias("User")
@Getter
@Setter
public class User {

    private String id;
    private String name;
    private String phone;
    private String email;
    private boolean useNickname;
    private String nickname;
    private int balance;
    private int rewards;

}
