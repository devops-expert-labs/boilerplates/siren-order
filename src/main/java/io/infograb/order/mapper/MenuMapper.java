package io.infograb.order.mapper;

import io.infograb.order.model.Menu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface MenuMapper {

    @Select("SELECT id, name, english_name, price FROM menu")
    List<Menu> selectMenus();

    @Select("SELECT id, name, english_name, price FROM menu WHERE id = #{id}")
    Menu selectMenuById(int id);

}
