package io.infograb.order.mapper;

import io.infograb.order.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface UserMapper {

    @Select("SELECT id, name, phone, email, use_nickname, nickname, balance, rewards FROM user WHERE id = #{id}")
    User selectUserById(String id);

    @Update("UPDATE user SET balance = #{balance}  WHERE id = #{id}")
    void updateBalance(@Param("id") String id, @Param("balance") int balance);

    @Update("UPDATE user SET rewards = #{rewards}  WHERE id = #{id}")
    void updateRewards(@Param("id") String id, @Param("rewards") int rewards);

}
