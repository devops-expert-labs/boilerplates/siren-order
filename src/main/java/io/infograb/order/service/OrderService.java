package io.infograb.order.service;

import io.infograb.order.mapper.MenuMapper;
import io.infograb.order.mapper.UserMapper;
import io.infograb.order.model.Menu;
import io.infograb.order.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class OrderService {

    private final UserMapper userMapper;

    private final MenuMapper menuMapper;

    @Autowired
    public OrderService(UserMapper userMapper, MenuMapper menuMapper) {
        this.userMapper = userMapper;
        this.menuMapper = menuMapper;
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public boolean orderDrink(String userId, int menuId) {
        User user = userMapper.selectUserById(userId);
        Menu menu = menuMapper.selectMenuById(menuId);
        int balance = user.getBalance();
        int price = menu.getPrice();
        if (balance - price >= 0) {
            userMapper.updateBalance(userId, balance - price);
            userMapper.updateRewards(userId, user.getRewards() + 1);
            return true;
        }
        return false;
    }

}
