package io.infograb.order.service;

import io.infograb.order.mapper.UserMapper;
import io.infograb.order.model.Menu;
import io.infograb.order.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class UserService {

    private final UserMapper userMapper;

    @Autowired
    public UserService(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    public User findUserById(String id) throws Exception {
        return userMapper.selectUserById(id);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void rechargeBalance(String userId, int amount) {
        User user = userMapper.selectUserById(userId);
        int balance = user.getBalance();
        userMapper.updateBalance(userId, balance + amount);
    }

}
